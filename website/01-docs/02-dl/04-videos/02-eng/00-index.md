---
layout: page
title: Видеокурсы по Deep Learning на английском языке
permalink: /dl/videos/eng/
---

# Видеокурсы по Deep Learning на английском языке



<br/>

## [Tensorflow](/dl/videos/eng/tensorflow/)

<br/>

## [Keras](/dl/videos/eng/keras/)


<br/>

### [Индус с ужасным акцентом и плохим микрофоном] Deep Learning with Real World Projects

![Deep Learning with Real World Projects](/img/video-courses/packtpub-deep-learning-with-real-world-projects.png "Deep Learning with Real World Projects"){: .center-image }


<br/>

### Deep Learning: Convolutional Neural Networks in Python

![Deep Learning: Convolutional Neural Networks in Python](/img/video-courses/convolutional-neural-networks-in-python.jpg "Deep Learning: Convolutional Neural Networks in Python"){: .center-image }

https://www.udemy.com/deep-learning-convolutional-neural-networks-theano-tensorflow/


<br/>

### Deep Learning with Java

![Deep Learning with Java](/img/video-courses/packtpub-deep-learning-with-java.png "Deep Learning with Java"){: .center-image }


<br/>

### Neural Networks Explained - Machine Learning Tutorial for Beginners

<br/> https://www.youtube.com/watch?v=GvQwE2OhL8I
<br/> https://www.youtube.com/watch?v=9ZsyQZouOQ8