---
layout: page
title: Видеокурсы по TensorFlow
permalink: /dl/videos/eng/tensorflow/
---


# Видеокурсы по TensorFlow


### [Tensorflow for Beginners](/dl/videos/eng/tensorflow/tensorflow-for-beginners/)


<br/>

### Complete Guide To TensorFlow For Deep Learning With Python

![Complete Guide To TensorFlow For Deep Learning With Python](/img/video-courses/TensorFlow.jpg "Complete Guide To TensorFlow For Deep Learning With Python"){: .center-image }


<br/>

### [Udemy] Deep Learning with TensorFlow 2.0 [2019]

![Deep Learning with TensorFlow 2.0](/img/video-courses/deep-learning-tensorflow-2-video.jpg "Deep Learning with TensorFlow 2.0"){: .center-image }

https://www.udemy.com/machine-learning-with-tensorflow-for-business-intelligence/


<br/>

### [LiveLessons] Deep Learning with TensorFlow: Applications of Deep Neural Networks to Machine Learning Tasks

https://www.oreilly.com/library/view/deep-learning-with/9780134770826/


https://github.com/the-deep-learners/TensorFlow-Livelessons

Хм. Буки 2-х летней давности.


<br/>

### Master Deep Learning with TensorFlow 2.0 in Python

![Master Deep Learning with TensorFlow 2.0 in Python](/img/video-courses/packtpub-master-deep-learning-with-tensorflow-in-python.png "Master Deep Learning with TensorFlow 2.0 in Python"){: .center-image }

