---
layout: page
title: Видеокурсы по Keras на английском языке
permalink: /dl/videos/eng/keras/
---

# Видеокурсы по Keras на английском языке

<br/>

### Practical Deep Learning with Keras and Python

![Practical Deep Learning with Keras and Python](/img/video-courses/packtpub-practical-deep-learning-with-keras-and-python.png "Practical Deep Learning with Keras and Python"){: .center-image }

https://github.com/PacktPublishing/-Practical-Deep-Learning-with-Keras-and-Python


<br/>

### [[Abhilash Nelson] Deep Learning and Neural Networks using Python – Keras: The Complete Beginners Guide [ENG (Араб), May 2019]](https://bitbucket.org/matematika/deep-learning-and-neural-networks-using-python-keras-the/src/master/)

Мне материал понравился. Возможно потому, что это первый видеокурс по keras который я посмотрел.

![Deep Learning and Neural Networks using Python – Keras](/img/video-courses/packtpub-keras-video.png "Deep Learning and Neural Networks using Python – Keras"){: .center-image }


<br/>

### Practical Projects with Keras 2.X

Плохое произношение на английском, лучше чем индусы или арабы, но всеравно чувствуется, что что-то не то.

Коды я не смог скачать с сайта packtput.
Мои коды <a href="https://bitbucket.org/matematika/practical-projects-with-keras-2.x/src/master/" rel="nofollow">здесь</a>

Если совсем с 0, лучше что-то другое посмотреть.

А вообще курс по моему оценочному суждению должен стоить не более $9.99, а они хотят за него аж $199.99

![Practical Projects with Keras 2.X](/img/video-courses/practical-projects-with-keras-2.png "Practical Projects with Keras 2.X"){: .center-image }


<br/>

### Keras 2.x Projects

![Keras 2.x Projects](/img/video-courses/keras-2x-projects-video.jpg "Keras 2.x Projects"){: .center-image }