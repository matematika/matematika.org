---
layout: page
title: Видеокурсы по Deep Learning на русском языке
permalink: /dl/videos/rus/
---

# Видеокурсы по Deep Learning на русском языке


<br/>

### [Otus] Deep Learning Engineer. Часть 1

Библиотека PyTorch

https://bitbucket.org/matematika/deep-learning-engineer/src/master/


Для запуска:


```
# {
    pip3 install --upgrade torch
    pip3 install --upgrade torchvision
}

# git clone https://matematika@bitbucket.org/matematika/deep-learning-engineer.git
```


<br/>

### Нейронные сети на Python (Часть 1)

Курс о нейронных сетях, глубоком машинном обучении и задачах, которые решает Deep Learning Инженер.

https://coursehunters.net/course/neyronnye-seti-na-python

**Если кто-то готов поделиться, welcome!**

<a href="https://konsultant.org/coursehunters-premium-skladchina.html">Если кто-то готов организовать совместную покупку премиум для скачивания материала с coursehunters</a>