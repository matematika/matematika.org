---
layout: page
title: Видеокурсы по Deep Learning
permalink: /dl/videos/
---

# Видеокурсы по Deep Learning


### [Видеокурсы по Deep Learning на русском языке](/dl/videos/rus/)

### [Видеокурсы по Deep Learning на английском языке](/dl/videos/eng/)