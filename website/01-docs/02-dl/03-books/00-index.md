---
layout: page
title: Книги по Deep Learning
permalink: /dl/books/
---

# Книги по Deep Learning


### [Книги по Deep Learning на русском языке](/dl/books/rus/)

### [Книги по Deep Learning на английском языке](/dl/books/eng/)