---
layout: page
title: Книги по Deep Learning на русском языке. Библиотека Keras
permalink: /dl/books/rus/keras/
---

# Книги по Deep Learning на русском языке. Библиотека Keras

<br/>

### [[Джулли А.,Пал С.] - Библиотека Keras - инструмент глубокого обучения - 2018](/dl/books/rus/keras/deep-learning-with-keras/)