---
layout: page
title: Джулли А.,Пал С. - Библиотека Keras - инструмент глубокого обучения - 2018
permalink: /dl/books/rus/keras/deep-learning-with-keras/
---

# [Джулли А.,Пал С.] - Библиотека Keras - инструмент глубокого обучения - 2018


**src:**  
https://github.com/PacktPublishing/Deep-Learning-with-Keras


**Цветные картинки к книге**  
https://static.packt-cdn.com/downloads/DeepLearningwithKeras_ColorImages.pdf

