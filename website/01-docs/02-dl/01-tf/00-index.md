---
layout: page
title: TensorFlow (Machine Learning)
permalink: /dl/tf/
---

# TensorFlow (Machine Learning)


![Tensor](/img/dl/tensor.png "Tensor"){: .center-image }



**Актуальная версия:**  
https://www.tensorflow.org/install/pip


<br/>

С версии 1.6 tf использует AVX инструкции. Поддерживает ваш проц такие или нет можно посмотреть в ubuntu здесь:

    $ cat /proc/cpuinfo

Чтобы использовать tf на проце, который не поддерживает AVX инструкции, нужно использовать версию 1.5.


<br/>

## Подготовка окружения

Можно самостоятелно установить.

![TensorFlow Install](/img/ml/videos/eng/tf/tensorflow-for-beginners/installation.png "TensorFlow Install"){: .center-image }


Пока планирую везде использовать docker.

<br/>

### [Запуск контейнера с TensorFlow в docker](/ml/devtools/docker/)

<br/>

**Узнать версию python3**

    from platform import python_version
    print(python_version())

**Узнать версию установленной библиотеки tensorflow**

    import tensorflow as tf
    print(tf.__version__)


<!--

# apt install -y curl git

# curl https://pyenv.run | bash

# pyenv install --list | grep " 3\.[678]"
  3.6.0
  3.6-dev
  3.6.1
  3.6.2
  3.6.3
  3.6.4
  3.6.5
  3.6.6
  3.6.7
  3.6.8
  3.6.9
  3.7.0
  3.7-dev
  3.7.1
  3.7.2
  3.7.3
  3.7.4
  3.8-dev


apt install -y libssl1.0-dev zlib1g-dev



$ pyenv install -v 3.5.2

$ pyenv versions

$ pyenv global 3.5.2
$ python -V

-->


<br/>

### Примеры:


https://github.com/GoogleCloudPlatform/training-data-analyst/blob/master/quests/scientific/coastline.ipynb

<br/>

Пример предсказания цены на дом

https://github.com/vijaykyr/tensorflow_teaching_examples/blob/master/housing_prices/cloud-ml-housing-prices.ipynb
