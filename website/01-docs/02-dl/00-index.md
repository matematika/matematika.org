---
layout: page
title: Deep Learning (Глубокое обучение) Нейронные сети
permalink: /dl/
---

# Deep Learning (Глубокое обучение) Нейронные сети 


<br/>

![Deep Learning](/img/dl/dl-01.png "Deep Learning"){: .center-image }

<br/>

![Deep Learning](/img/dl/dl-02.png "Deep Learning"){: .center-image }

<br/>

![Deep Learning](/img/dl/dl-03.png "Deep Learning"){: .center-image }

<br/>

![Deep Learning](/img/dl/dl-04.png "Deep Learning"){: .center-image }

<br/>

![Deep Learning](/img/dl/dl-05.png "Deep Learning"){: .center-image }

<br/>

![Deep Learning](/img/dl/dl-06.png "Deep Learning"){: .center-image }

<br/>

![Deep Learning](/img/dl/dl-07.png "Deep Learning"){: .center-image }


<br/>

## Библиотеки грубокого обучения

<br/>

### [TensorFlow](/dl/tf/)

TensorFlow - библиотека от Google для ML/DL. Можно запускать обычным способом, в docker контейнерах, в том числе в kubernetes кластерах (инструмент для запуска кучи контейнеров на куче серверов). Есть специальный проект kubeflow, для работы tensorflow в kubernetes. Скрипты для развертывания локального kubernetes кластера в linux у меня есть. Спросить в чате, если нужно.

TensorFlow, вроде как, был сложен для программистов и была написана библиотека упрощающая работу с ним - keras. Библиотека набрала популярность и в Google решили добавить наработки из этой библиотеки во вторую версию TensorFlow, которая пока beta. В общем, если выбирать библиотеку для изучения DL, наверное лучше сразу начинать копаться со второй версии TensorFlow.

<br/>

### [Keras](/dl/keras/)

Работает над TensorFlow или дургой библиотекой Theano. 


<br/>

### PyTorch

Вроде как, "более гибкая" библиотека, чем keras. Так сказал автор курса от Отус по ML.

<br/>

### Lasagna (построена на основе библиотеки theano)

Хз, что за библиотека такая

<br/>

## Обучающие материалы:

### [Книги по Deep Learning](/dl/books/)

### [Видеокурсы по Deep Learning](/dl/videos/)


<br/>

### Practical Deep Learning for Coders 

https://github.com/fastai/courses