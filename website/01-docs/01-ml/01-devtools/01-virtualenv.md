---
layout: page
title: Virtualenv
permalink: /ml/devtools/virtualenv/
---

# Virtualenv

    $ sudo apt-get update && sudo apt-get upgrade
    $ sudo apt-get install -y python3.7
    $ sudo apt install python3-pip

    $ python3 --version
    Python 3.6.8

<!--
# $ pip3 install -U virtualenv
-->

    $ sudo apt install -y virtualenv
    $ virtualenv --system-site-packages -p python3 tf_2
    $ source tf_2/bin/activate

    $ pip install --upgrade pip
    $ pip install --upgrade tensorflow==2.0.0-beta1

    
```(tf_2) $ python -c "import tensorflow as tf; x = [[2.]]; print('tensorflow version', tf.__version__); print('hello, {}'.format(tf.matmul(x, x)))"
```

    $ pip install jupyterlab

    $ jupyter notebook --ip 0.0.0.0 --port 8888


<br/>

```

from platform import python_version
print(python_version())

import tensorflow as tf
print(tf.__version__)

```

Все ок.