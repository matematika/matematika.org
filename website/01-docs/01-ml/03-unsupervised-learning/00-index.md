---
layout: page
title: Обучение без учителя (Unsupervised Learning)
permalink: /ml/unsupervised-learning/
---

# Обучение без учителя (Unsupervised Learning)

* Кластеризация (Clustering)
* Неконтролируемые преобразования (unsupervised transformations) (Общераспространенное применение неконтролируемых преобразований – Сокращение размерности (Dimensionality Reduction))