---
layout: page
title: Видеокурсы по машинному обучению на английском языке
permalink: /ml/videos/eng/
---

# Видеокурсы по машинному обучению на английском языке

<br/>

## [Scikit-learn](/ml/videos/eng/scikit-learn/)


<br/>

## Остальное

<br/>

### [Араб] OpenCV Complete Dummies Guide to Computer Vision with Python


<br/>

### Machine Learning | Complete Project In Credit Card Fraud Detection | Eduonix

https://www.youtube.com/watch?v=9cXeEwOXWU8


<br/>

### Machine Learning (Stanford) 2008

https://www.youtube.com/watch?v=UzxYlbK2c7E&list=PLA89DCFA6ADACE599

<br/>

### Machine Learning Maths

<br/> https://www.youtube.com/watch?v=QDpeRUIrb6U&list=PLDmvslp_VR0wU8LBWchfpR6rp38TzQbAO



<br/>

### Learn Linear Regression in Python [ENG]

https://www.youtube.com/watch?v=DLcxu7N757Y&list=PLDmvslp_VR0znrMIwo4zQMoMU9ZRT4c3i

<br/>

## Natural Language Processing (NLP)

<br/>

### Natural Language Processing (NLP) [ENG]

https://www.youtube.com/watch?v=z6viESbVsa0

<br/>

### NLP – Natural Language Processing with Python