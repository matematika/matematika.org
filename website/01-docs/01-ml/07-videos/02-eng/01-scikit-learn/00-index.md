---
layout: page
title: Scikit-learn
permalink: /ml/videos/eng/scikit-learn/
---

# Scikit-learn

### Machine Learning 101 with Scikit-learn and StatsModels

![Machine Learning 101 with Scikit-learn and StatsModels](/img/video-courses/ml/machine-learning-101-scikit-learn-statsmodels.jpg "Machine Learning 101 with Scikit-learn and StatsModels"){: .center-image }


<br/>

### [[Abhilash Nelson] Machine Learning and Data Science with Python: A Complete Beginners Guide [ENG (Араб), May 2019]](https://bitbucket.org/matematika/machine-learning-and-data-science-with-python-a-complete/src/master/)

![Machine Learning and Data Science with Python: A Complete Beginners Guide](/img/video-courses/machine-learning-data-science-python-guide.png "Machine Learning and Data Science with Python: A Complete Beginners Guide"){: .center-image }

https://www.packtpub.com/eu/application-development/machine-learning-and-data-science-python-complete-beginners-guide-video


<br/>

### [Machine Learning Fundamentals](/ml/videos/eng/scikit-learn/machine-learning-fundamentals/)


<br/>

### Projects in Machine Learning : Beginner To Professional

https://www.udemy.com/machine-learning-for-absolute-beginners/

<br/>

### Machine Learning Classification Bootcamp in Python (2019)

<br/>

### Building Recommender Systems with Machine Learning and AI (2019)

![Building Recommender Systems with Machine Learning and AI (2019)](/img/video-courses/building-recommender-systems.jpg "Building Recommender Systems with Machine Learning and AI (2019)"){: .center-image }

