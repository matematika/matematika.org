---
layout: page
title: Обучение с учителем (Supervised Learning)
permalink: /ml/supervised-learning/
---

# Обучение с учителем (Supervised Learning)

  - <a href="/ml/supervised-learning/classification/">Классификация (Classification)</a>
  - <a href="/ml/supervised-learning/regression/">Регрессия (Regression)</a>
  - Определение аномалий (Anomaly detection)