---
layout: page
title: Регрессия (Regression)
permalink: /ml/supervised-learning/regression/
---

# Регрессия (Regression)

**Simple Linear Regression. Minimal example**

https://bitbucket.org/matematika/deep-learning-with-tensorflow-2.0/src/master/01-Simple%20Linear%20Regression.%20Minimal%20example.ipynb

<br/>

**Simple Linear Regression. Minimal example with TensorFlow 2.0**

https://bitbucket.org/matematika/deep-learning-with-tensorflow-2.0/src/master/02-Simple%20Linear%20Regression.%20Minimal%20example%20with%20TensorFlow%202.0.ipynb