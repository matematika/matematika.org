---
layout: page
title: Книги по машинному обучению на английском языке
permalink: /ml/books/eng/
---

# Книги по машинному обучению на английском языке


Поскольку математика (в частности, теория вероятностей) является той основой, на которой строится машинное обучение, мы не будем детально вдаваться в подробности алгоритмов. Если вас интересует математический аппарат алгоритмов машинного обучения, мы рекомендуем книгу издательства Springer The Elements of Statistical Learning за авторством Тревора Хасти, Роберта Тибширани и Джерома Фридмана, которая свободно доступна на  <a href="https://web.stanford.edu/~hastie/ElemStatLearn/" rel="nofollow">сайте авторов</a>.

<br/>

<a href="https://web.stanford.edu/~hastie/Papers/ESLII.pdf" rel="nofollow">pdf</a>


<br/>

<a href="http://www.cin.ufpe.br/~embat/Python%20for%20Data%20Analysis.pdf" rel="nofollow">Python for Data Analysis</a>
