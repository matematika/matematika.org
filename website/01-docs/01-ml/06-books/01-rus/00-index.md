---
layout: page
title: Книги по машинному обучению на русском языке
permalink: /ml/books/rus/
---

# Книги по машинному обучению на русском языке


    - Почему книги по программированию на русском?
    - Да тут и на русском не разобрать!


<br/>

## Scikit-learn

### [Мюллер.А, Гвидо. С. - Введение в машинное обучение с помощью Python. Руководство для специалистов по работе с данными [RUS, 2017]](/ml/books/rus/scikit-learn/introduction-to-ml-with-python/)

<br/>

Рашка С. - Python и машинное обучение [RUS, 2017]


<br/>

## TensorFlow

### [[Шакла Нишант] - Машинное обучение и TensorFlow (Библиотека программиста) - 2019](/ml/books/rus/tf/machine-learning-with-tensorflow/)


