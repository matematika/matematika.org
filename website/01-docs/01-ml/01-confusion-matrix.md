---
layout: page
title: Confusion matrix
permalink: /ml/confusion-matrix/
---

# Confusion matrix


<br/>

![Confusion matrix](/img/ml/confusion-matrix-01.png "Confusion matrix"){: .center-image }

<br/>

![Confusion matrix](/img/ml/confusion-matrix-02.png "Confusion matrix"){: .center-image }

<br/>

![Confusion matrix](/img/ml/confusion-matrix-03.png "Confusion matrix"){: .center-image }

<br/>

![Confusion matrix](/img/ml/confusion-matrix-04.png "Confusion matrix"){: .center-image }

