---
layout: page
title: Машинное обучение (Machine Learning)
permalink: /ml/
---

# Машинное обучение (Machine Learning)

Я только приступаю и пытаюсь разобраться что к чему. 

Наверное, лучше всего приступать к изучению Machine Learning с книги [Мюллер.А, Гвидо. С. - Введение в машинное обучение с помощью Python. Руководство для специалистов по работе с данными [RUS, 2017]](/ml/books/rus/scikit-learn/introduction-to-ml-with-python/) и видеокурса [Udemy] Deep Learning with TensorFlow 2.0 [2019]. Возможно перед этим видеокурсом стоит посмотреть видео [[Abhilash Nelson] Machine Learning and Data Science with Python: A Complete Beginners Guide [ENG, May 2019]](https://bitbucket.org/matematika/machine-learning-and-data-science-with-python-a-complete/src/master/). Какие-то другие материалы, воспринимались намного хуже.

Все это добро можно купить, а если вы такой же жадина на траты всяких обучающих материалов как и я, то я думаю и у вас получится найти все это в интернете на файлопомойках и торрентах.

Предполагается, что вы обладаете знаниями по основам программирования. Конкретно в этом случае, используется Python. 


<br/>

![Machine Learning](/img/ml/pic1.png "Machine Learning"){: .center-image }

<br/>

* <a href="/ml/supervised-learning/">Обучение с учителем (Supervised Learning)</a>
* <a href="/ml/unsupervised-learning/">Обучение без учителя (Unsupervised Learning)</a>
* <a href="/ml/reinforcement-learning/">Обучение с подкреплением (Reinforcement Learning)</a>


<br/>

![Machine Learning](/img/ml/pic2.png "Machine Learning"){: .center-image }

<br/>

![Machine Learning](/img/ml/pic3.png "Machine Learning"){: .center-image }

<br/>

![Machine Learning](/img/ml/pic4.png "Machine Learning"){: .center-image }


<br/>

![Machine Learning](/img/ml/ml-04.png "Machine Learning"){: .center-image }

<br/>

![Machine Learning](/img/ml/ml-05.png "Machine Learning"){: .center-image }

<br/>

![Machine Learning](/img/ml/ml-06.png "Machine Learning"){: .center-image }


<!--
  https://ods.ai/
-->


<br/>

### Дистрибутивы

Тяжелая <a href="/ml/devtools/anaconda/">Anaconda</a>, в которой уже все установлено и настроено.

Как по мне, лучше сразу использовать <a href="/ml/devtools/docker/">Docker</a>

Или самостоятельно сделать свое <a href="/ml/devtools/virtualenv/">виртульное окружение</a>


<br/>

### Данные для ML

* Kaggle содержит все типы крупномасштабных данных для машинного об­
учения: www.kaggle.com/datasets.
* Data.gov является базой данных, которая была открыта по инициативе
правительства США, и в ней содержится много интересных и представляю­
щих практическую ценность наборов данных: https://catalog.data.gov
* http://archive.ics.uci.edu/ml/datasets/


<br/>

Для Recommender systems оценки фильмов 

* https://grouplens.org/datasets/movielens/

<br/>

## Библиотеки:

Собственно за нас уже все сдалали, нужно научиться пользоваться.

### [Scikit-learn](/ml/scikit-learn/)

Для глубокого обучения scikit-learn подходит хуже. Библиотеки вроде tensorflow  позволяют использовать высокопроизводительные графические процессоры (GPU).

Изучение библиотек глубокого обучения вынес  <a href="/dl/">сюда</a>


<br/>

### Библиотеки от Apache

* Spark MLlib


<br/>

## Обучающие материалы:

### [Книги по машинному обучению](/ml/books/)

### [Видеокурсы по машинному обучению](/ml/videos/)

<br/>

## Github

Екатерина Тузова - https://github.com/ktisha/ML2018

http://mit.spbau.ru/sewiki/index.php/%D0%9C%D0%B0%D1%88%D0%B8%D0%BD%D0%BD%D0%BE%D0%B5_%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5_2018


<br/>

### [Confusion matrix](/ml/confusion-matrix/)

<br/>

### [K-Fold](/ml/k-fold/)


