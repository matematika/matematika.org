---
layout: page
title: R
permalink: /r/
---

# R

<br/>

### R Notes for Professionals

https://goalkicker.com/RBook/


<br/>

### Data Science Masterclass With R! 4 Projects+8 Case Studies

https://www.udemy.com/data-science-complete-course/

![Data Science Masterclass With R! 4 Projects+8 Case Studies](/img/video-courses/Data-Science-Masterclass-With-R-4-Projects8-Case-Studies.jpg "Data Science Masterclass With R! 4 Projects+8 Case Studies"){: .center-image }


<br/>

### R Tutorial | Learn Data Joins| Eduonix

https://www.youtube.com/watch?v=JvYfaMu-M7o