---
layout: page
title: Видеокурсы по BigData
permalink: /bigdata/videos/
---

# Видеокурсы по BigData


### [Видеокурсы по BigData на русском языке](/bigdata/videos/rus/)

### [Видеокурсы по BigData на английском языке](/bigdata/videos/eng/)