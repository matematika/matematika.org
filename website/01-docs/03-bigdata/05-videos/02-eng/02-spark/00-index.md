---
layout: page
title: Spark
permalink: /bigdata/videos/eng/spark/
---

# Spark



<br/>

### Scala and Spark for Big Data and Machine Learning

![Scala and Spark for Big Data and Machine Learning](/img/video-courses/Scala-and-Spark-for-Big-Data-and-Machine-Learning.jpg "Scala and Spark for Big Data and Machine Learning"){: .center-image }

https://www.udemy.com/scala-and-spark-for-big-data-and-machine-learning/


<br/>

### Spark in Action Video Edition

Как-то здесь все уныло. В основном читает текст с экрана. Не досмотрел.

<a href="/bigdata/videos/spark/spark-in-action-video-edition/">сюда</a>.

![Spark in Action Video Edition](/img/video-courses/Spark-in-Action-Video-Edition.jpg "Spark in Action Video Edition"){: .center-image }

<br/>

### Apache Spark for Java Developers

Прям в начале курса говорят, что обычно используют scala. 

![Apache Spark for Java Developers](/img/video-courses/apache-spark-for-java-developers-video.jpg "Apache Spark for Java Developers"){: .center-image }

https://www.udemy.com/apache-spark-for-java-developers/