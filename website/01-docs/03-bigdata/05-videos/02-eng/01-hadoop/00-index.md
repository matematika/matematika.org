---
layout: page
title: Hadoop
permalink: /bigdata/videos/eng/hadoop/
---

# Hadoop


<br/>

### The Ultimate Hands-On Hadoop – Tame Your Big Data!

<a href="/bigdata/videos/eng/hadoop/the-ultimate-hands-on-hadoop/">сюда</a>.

![hadoop](/img/video-courses/hadoop.jpg "hadoop"){: .center-image }




<br/>

### Hadoop and Spark Fundamentals

![Hadoop and Spark Fundamentals](/img/video-courses/hadoop-spark-fundamentals.jpg "Hadoop and Spark Fundamentals"){: .center-image }