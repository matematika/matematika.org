---
layout: page
title: BigData
permalink: /bigdata/
---

# BigData

Вроде как есть бесплатное облачное решение на AWS для тестов spark в облаках. Т.е. самому ничего не нужно настраивать. Аналог jupyter notebook (если я все правильно понимаю). Не смог зарегиться, кнопка регистрации не реагировала на клик после заполнения полей.  
https://databricks.com/try-databricks

Разумеется, пратное решение будет работать.

<br/>

### Jupyter notebook для scala/spark

    $ docker run -p 8888:8888 jupyter/all-spark-notebook


<br/>

### Zeppelin в docker контейнере

Не разобрался как добавить репо. Поэтому не особо рекомендую. Но scala/spark код выполнять можно.

    $ docker pull apache/zeppelin:0.8.1

    $ docker run --rm -it -p 7077:7077 -p 8080:8080 apache/zeppelin:0.8.1

http://localhost:8080

<br/>

### [JDK installation in linux (Ubuntu, Centos)](https://javadev.org/devtools/jdk/install/linux/)

### [Installation SCALA in linux](https://javadev.org/devtools/scala/install/linux/)

### [Apache Hadoop, Pig, Hive, Derby installation in Centos Linux](https://javadev.org/devtools/hadoop/install/linux/)

### [Apache Spark installation in Linux](https://javadev.org/devtools/spark/install/linux/)



<br/>

## Обучающие материалы:

### [Книги по BigData](/bigdata/books/)

### [Видеокурсы по BigData](/bigdata/videos/)
