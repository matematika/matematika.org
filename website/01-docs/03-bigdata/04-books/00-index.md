---
layout: page
title: Книги по BigData
permalink: /bigdata/books/
---

# Книги по BigData


### [Книги по BigData на русском языке](/bigdata/books/rus/)

### [Книги по BigData на английском языке](/bigdata/books/eng/)