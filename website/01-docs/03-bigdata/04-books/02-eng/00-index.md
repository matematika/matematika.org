---
layout: page
title: Книги по BigData на английском языке
permalink: /bigdata/books/eng/
---

# Книги по BigData на английском языке


![Книги по BigData на английском языке](/img/bigdata/books/eng/big-data-books.png "Книги по BigData на английском языке"){: .center-image }

<br/>


Big Data Recommender Systems – Volume 1: Algorithms, Architectures, Big Data, Security and Trust

Big Data Recommender Systems – Volume 2: Application Paradigms
