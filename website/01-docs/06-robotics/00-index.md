---
layout: page
title: Robotics
permalink: /robotics/
---

# Robotics

<br/>

### Robotic Process Automation: RPA Fundamentals and Build a Robot

<br/>

### Become a Robotics Engineer. Acquire hands-on experience with projects fusing artificial intelligence and hardware.