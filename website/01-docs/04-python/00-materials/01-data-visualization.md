---
layout: page
title: Python Data Visualization
permalink: /python/data-visualization/
---

# Python Data Visualization

<br/>

### Learn Data Visualization [ENG]

https://www.youtube.com/playlist?list=PLDmvslp_VR0z6rAWW_wuDjWRqwphI_cIu

<br/>

### Python Data Visualization With Bokeh [ENG]

https://www.youtube.com/watch?v=2TR_6VaVSOs
