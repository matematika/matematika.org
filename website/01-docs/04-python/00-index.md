---
layout: page
title: Python для анализа данных
permalink: /python/
---

# Python для анализа данных

<br/>

### [Python Data Visualization](/python/data-visualization/)

### [Python Web Scraping](/python/web-scrapping/)

<br/>

### Математика и Python для анализа данных

https://www.coursera.org/learn/mathematics-and-python/

<br/>

## Видел в интернете (Чтобы не потерять):

- Learning Python for Data Analysis and Visualization
  https://www.udemy.com/learning-python-for-data-analysis-and-visualization/

- Data Visualisation with Plotly and Python

  https://www.udemy.com/data-visualisation-with-plotly-and-python/

* Python for Finance: Investment Fundamentals & Data Analytics
