---
layout: page
title: Планы по изучению материалов
permalink: /plans/
---

# Планы по изучению материалов


<br/>

## Видео


<br/>

### [Udemy] Deep Learning with TensorFlow 2.0 [2019]

https://www.udemy.com/machine-learning-with-tensorflow-for-business-intelligence/

Презентация интересная. Начало тоже. Определенно лучше материалов индусов, арабов. Начало смотрится как мультфильм.


<br/>

### [Otus] Deep Learning Engineer. Часть 1

Библиотека PyTorch


<br/>

### [Otus] Нейронные сети на Python (Часть 1)

Курс о нейронных сетях, глубоком машинном обучении и задачах, которые решает Deep Learning Инженер.

https://coursehunters.net/course/neyronnye-seti-na-python

**Если кто-то готов поделиться, welcome!**

<a href="https://konsultant.org/coursehunters-premium-skladchina.html">Если кто-то готов организовать совместную покупку премиум для скачивания материала с coursehunters</a>

<br/>

### Hadoop and Spark Fundamentals

![Hadoop and Spark Fundamentals](/img/video-courses/hadoop-spark-fundamentals.jpg "Hadoop and Spark Fundamentals"){: .center-image }


<br/>

### [Tomasz Lelek] - материалы по Hadoop, Spark, etc.


Томас более 5 лет работает на продах по направлению, котороя я только собираюсь осваивать.

https://www.packtpub.com/authors/tomasz-lelek


**Найдено:**

* Apache Spark: Tips, Tricks, & Techniques
* Hands-On Machine Learning with Scala and Spark 
* Mastering Deep Learning using Apache Spark
* Hands-On Big Data Analysis with Hadoop 3 
* Microservices in Scala
* Deep Learning with Apache Spark
* Machine Learning Projects with Java
* Spark for Machine Learning
* Advanced Analytics and Real-Time Data Processing in Apache Spark
* Solving 10 Hadoop'able Problems
* Identifying Behaviour Patterns using Machine Learning
* Distributed Deep Learning with Apache Spark
* Building a Big Data Analytics Stack
* Big Data Analytics Projects with Apache Spark
* Big Data Processing using Apache Spark
* Troubleshooting Apache Spark
* Learning Apache Cassandra 

<br/>

**Пока отсутствуют:**

* Real Time Streaming using Apache Spark 

Если у кого есть премиум на rapidgator, можете помочь скачать файл, доступный только для премиальных пользоватлей. Смотри исходники этой страницы.

<!--

https://rapidgator.net/file/a95ed50e67f30670c7bfcd2a22ebd942/Packt_Publishing_-_Real_Time_Streaming_using_Apache_Spark_Streaming.rar.html

-->

<br/>

Материалы пока не смотрел. Там может быть и ерунда какая. Packtpub обычно какой-то шлак под своим брендом собирает. Я ориентировался по заголовкам. Но зато есть исходные коды с примерами.

Буду признателен на ссылки на материалы и других специалистов-практиков.


<br/>

## Книги

### <a href="/ml/books/rus/scikit-learn/introduction-to-ml-with-python/">Мюллер.А, Гвидо. С. - Введение в машинное обучение с помощью Python. Руководство для специалистов по работе с данными [RUS, 2017]</a>

### Шакла Нишант - Машинное обучение и TensorFlow (Библиотека программиста) - 2019

### Джулли А.,Пал С. - Библиотека Keras - инструмент глубокого обучения - 2018


<br/><br/>

# Изученные материалы

## Видео

<br/>

### [[Abhilash Nelson] Machine Learning and Data Science with Python: A Complete Beginners Guide [ENG (Араб), May 2019]](https://bitbucket.org/matematika/machine-learning-and-data-science-with-python-a-complete/src/master/)

![Machine Learning and Data Science with Python: A Complete Beginners Guide](/img/video-courses/machine-learning-data-science-python-guide.png "Machine Learning and Data Science with Python: A Complete Beginners Guide"){: .center-image }


<br/>

### [[Abhilash Nelson] Deep Learning and Neural Networks using Python – Keras: The Complete Beginners Guide [ENG (Араб), May 2019]](https://bitbucket.org/matematika/deep-learning-and-neural-networks-using-python-keras-the/src/master/)


![Deep Learning and Neural Networks using Python – Keras](/img/video-courses/packtpub-keras-video.png "Deep Learning and Neural Networks using Python – Keras"){: .center-image }



<br/>

### Scala and Spark for Big Data and Machine Learning

Здесь почти нет ничего по BigData. В основном про ML.

![Scala and Spark for Big Data and Machine Learning](/img/video-courses/Scala-and-Spark-for-Big-Data-and-Machine-Learning.jpg "Scala and Spark for Big Data and Machine Learning"){: .center-image }

https://www.udemy.com/scala-and-spark-for-big-data-and-machine-learning/