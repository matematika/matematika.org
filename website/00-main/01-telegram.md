---
layout: page
title: Telegram группа (чат)
permalink: /telegram/
---

# Telegram группа (чат)


**Группа в телеграм:** matematika_org

У кого сайт телеграма незаблокирован (а это у всех жувущих за пределами РФ), можно посмотреть что происходит в чате (а там ничего не происходит) по ссылке: https://t.me/matematika_org

Пока людей совсем мало. Если будет побольше, будет "talk". Можно сидеть молча и ни с кем не разговаривать, пока не появится тема интересующая лично вас. 

Телеграм нужен для обмена информацией по всякой информационной халяве, координации коллективного скачивания с файлопомоек и торрентов нужных материалов, обмена заниями (которые у админа почти отсутствуют), обсуждения работы, вакансий, примеров, собственно сайта и материалов размещенных на нем.