---
layout: page
title: Карта Сайта
permalink: /sitemap/
---

# Карта Сайта

<br/>

### [Машинное обучение (Machine Learning)](/ml/)

### [Глубокое обучение(Deep Learning)](/dl/)

### [Big Data](/bigdata/)

### [BlockChain](/blockchain/)

### [Линейная алгебра](/linal/)

### [Математический анализ](/matan/)

### [Теория вероятности](/terver/)

### [Python для анализа данных](/python/)

### [Robotics](/robotics/)


<br/><br/>

ML, Тервер

https://www.youtube.com/user/mathematicalmonk/playlists



<br/>

### MATLAB Tutorial

<br/> https://www.youtube.com/watch?v=NSSTkkKRabI

<br/>

### Опойцев Валерий Иванович (Необходимо классифицировать)

https://oschool.ru/

https://www.youtube.com/channel/UCRna3TsezxOptinsv-4ILzA/playlists

<br/>

<br/>

### [Позитивное видео про математиков (История Перельмана)](/videos/)
