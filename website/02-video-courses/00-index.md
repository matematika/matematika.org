---
layout: page
title: Видеокурсы по ML, BIG Data, DS
permalink: /video-courses/
---

# Видеокурсы по ML, BIG Data, DS

<br/>

Записываю, т.к. уже не могу запомнить что уже было найдено

<br/>

### [Видеокурсы по ML](/ml/videos/eng/)

### [Видеокурсы по BigData (Hadoop, Spark)](/bigdata/videos/eng/)

### [Видеокурсы по Data Science](/video-courses/ds/)

### [Видеокурсы по математике](/video-courses/matematika/)
