---
layout: page
title: Видеокурсы по Data Science
permalink: /video-courses/ds/
---

# Видеокурсы по Data Science


<br/>

### Python for Data Science

![Python for Data Science](/img/video-courses/python-for-datascience.jpg "Python for Data Science"){: .center-image }

<br/>

### Data Science Fundamentals Part 1: Learning Basic Concepts, Data Wrangling, and Databases with Python

<br/>

![Data Science Fundamentals Part 1](/img/video-courses/Data_Science_Fundamentals-1.jpeg "Data Science Fundamentals Part 1"){: .center-image }

<br/>

### Data Science Fundamentals Part 2: Machine Learning and Statistical Analysis

<br/>

![Data Science Fundamentals Part 2](/img/video-courses/Data_Science_Fundamentals-2.jpeg "Data Science Fundamentals Part 2"){: .center-image }

<br/>

### Complete Data Science & Machine Learning Bootcamp – Python 3

<br/>

![Complete Data Science & Machine Learning Bootcamp – Python 3](/img/video-courses/python-data-science-machine-learning-bootcamp.jpg "Complete Data Science & Machine Learning Bootcamp – Python 3"){: .center-image }

